mmcore.IntegrationFactory.register('DecibelInsight', {
    defaults: {
        modifiers: {
            Persist: function(data, buildParams){
                if (data.persist && (data.persistentCount || this.defaults.persistentCount)) {
                    try {
                        var storedList = JSON.parse(mmcore.GetCookie('mm_iPersist', 1) || '[]');
                    } catch (e) {
                        var storedList = [];
                    };

                    var searchFor = data.campaign + '=';
                    for (var i = storedList.length; i --;) {
                        if(!(storedList[i].indexOf(searchFor))) {
                            storedList.splice(i, 1);
                        }
                    }
                    storedList.push(data.campaignInfo);
                    while(storedList.length > data.persistentCount)
                        storedList.shift();

                    mmcore.SetCookie('mm_iPersist', JSON.stringify(storedList), 365, 1);
                    data.campaignInfo = storedList.join('&');
                }
            }
        },
        persistentCount: 15
    },

    validate: function(data){
        if(!data.campaign)
            return 'No campaign.';
        return true;
    },

    check: function(data){
        return window.decibelInsight;
    },

    exec: function(data){
        var prodSand = data.isProduction ? 'MM_Prod' : 'MM_Sand',
            info = prodSand + '_' + data.campaignInfo;

        decibelInsight('ready', function () {
            decibelInsight('sendIntegrationData', 'Maxymiser', info);
        });

        if(data.callback) data.callback();
        return true;
    }
});