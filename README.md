# DecibelInsight

---

[TOC]

## Introduction

Register and initialize DecibelInsight integration.

## Restrictions

+ No more than 10 experiences per campaign

    (You can have more than 10 experiences, but in this case please be sure that you've covered all them in Segmentation Rules. Please read more in __Deployment Instructions__ section)

+ No support for redirect campaigns

+ No support for campaigns inside the Master campaign

## Download

* [decibelinsight-register.js](https://bitbucket.org/mm-global-se/if-decibelinsight/src/master/src/decibelinsight-register.js)

* [decibelinsight-initialize.js](https://bitbucket.org/mm-global-se/if-decibelinsight/src/master/src/decibelinsight-initialize.js)

## Ask client for

+ Campaign name

## Deployment Instructions

This integration requires some additional steps

+ Ensure that you have Integration Factory plugin deployed on site level site wide and it has _order: -10_ ([find out more](https://bitbucket.org/mm-global-se/integration-factory))

+ Create another site script (mapped site wide) with _order: -5_ and add the code from [decibelinsight-register.js](https://bitbucket.org/mm-global-se/if-decibelinsight/src/master/src/decibelinsight-register.js) into it

+ Create campaign script with _order: > -5_, then customize the code from [decibelinsight-initialize.js](https://bitbucket.org/mm-global-se/if-decibelinsight/src/master/src/decibelinsight-initialize.js) and add into it

+ Create new Personalization Criterion with the name "CurrentCustomer" of "Free Text" type

+ Go to your target campaign

+ Create Segment Rules for each possible campaign experience

  * Choose "CurrentCustomer" criterion from Personalization Criteria Tab

    ![di_1.png](https://bitbucket.org/repo/G9XnRb/images/3762728350-di_1.png)

  * Enter value for an experience in the next format: `Campaign=element1:Default|element2:variant`

    (Please note! It should be the same as Integration Factory output for particular experience)

    ![di_2.png](https://bitbucket.org/repo/G9XnRb/images/1475219329-di_2.png)

  * Configure corresponding experience to be shown

    ![di_4.png](https://bitbucket.org/repo/G9XnRb/images/2931182000-di_4.png)

## QA

Access to corresponding DecibleInsight account required